(function ($) {

Drupal.behaviors.nqp = {
  attach: function(context) {
    var title = $('input[name="title"]');

    // Update the title in Nodequeue Settings when the title changes and
    // when the page first loads.
    title.bind('change input keyup paste', function () {
      var titleVal = (title.val()) ? title.val() : Drupal.t('(Untitled)');
      $('li.nqp-node-title-existing a, li.nqp-node-title-added').text(titleVal);
    }).trigger('change');

    // Update a queue list when the queue checkbox is changed and
    // when the page first loads.
    $('input.nqp-node-enabled').bind('change', function () {
      var qname_matches = $(this).attr('name').match('^nqp_(.+)_enabled$');
      var qname = qname_matches[1];
      var titleVal = (title.val()) ? title.val() : Drupal.t('(Untitled)');
      queue = $(this).closest('div.nqp-queue');
      nqp_update_list(queue, qname, titleVal);
    }).trigger('change');
  }
};

Drupal.behaviors.nqpFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-nodequeue-publish', context).drupalSetSummary(function(context) {
      var nqp_queue_list = '';
      $('div.nqp-queue').each(function() {
        if ($(this).find('input.nqp-node-enabled').is(':checked')) {
          if (nqp_queue_list == '') {
            nqp_queue_list += $(this).find('span.nqp-queue-title').text();
          } else {
            nqp_queue_list += ', ' + $(this).find('span.nqp-queue-title').text();
          }
        }
      });
      if (nqp_queue_list == '') {
        return Drupal.t('Not included in queues');
      } else {
        return Drupal.t('Included in: ') + nqp_queue_list;
      }
    });
  }
};

function nqp_update_list(queue, qname, nodeTitle) {
  var settings = Drupal.settings.nqp[qname];
  var queueSize = settings.queue['size'];
  var queueReverse = settings.queue['reverse'];
  var queueCount = settings.queue['count'];
  var nodeTitleExisting = queue.find('li.nqp-node-title-existing');
  var nodeTitleNew = queue.find('li.nqp-node-title-added');
  var queueTitles = queue.find('div.nqp-node-titles ul li');
  var queueEmpty = queue.find('div.nqp-queue-empty');

  if ($(queue).find('input.nqp-node-enabled').is(':checked')) {
    if (nodeTitleExisting.hasClass('nqp-node-title-existing')) {
      nodeTitleExisting.removeClass('nqp-node-title-removed');
    } else {
      if (queueEmpty.text()) {
        queueEmpty.remove();
        queue.find('div.nqp-node-titles').append("<ul>\n<li class=\"nqp-node-title-added\">" + nodeTitle + "</li>\n</ul>");
      }
      if (queueReverse == 1) {
        queueTitles.first().before($('<li>' + nodeTitle + '</li>').addClass('nqp-node-title-added'));
        if (queueSize > 0 && queueCount + 1 > queueSize) {
          queueTitles.last().addClass('nqp-node-title-removed');
        }
      } else {
        queueTitles.last().after($('<li>' + nodeTitle + '</li>').addClass('nqp-node-title-added'));
        if (queueSize > 0 && queueCount + 1 > queueSize) {
          queueTitles.first().addClass('nqp-node-title-removed');
        }
      }
    }
  } else {
    nodeTitleNew.remove();
    if (queueCount == 0) {
      queue.find('div.nqp-node-titles ul').remove();
      if (! queueEmpty.text()) {
        queue.find('div.nqp-node-titles').append('<div class="nqp-queue-empty">(' + Drupal.t('This queue is empty') + ')</div>');
      }
    }
    queueTitles.removeClass('nqp-node-title-removed');
    nodeTitleExisting.addClass('nqp-node-title-removed');
  }
}

}) (jQuery);
